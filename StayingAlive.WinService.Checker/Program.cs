﻿using System;
using Akka.Actor;

namespace StayingAlive.WinService.Checker
{
    //public class HealthCheck
    //{
    //    public HealthCheck(string fileName, string reason)
    //    {
    //        FileName = fileName;
    //        Reason = reason;
    //    }

    //    public string FileName { get; private set; }

    //    public string Reason { get; private set; }
    //}
    class Program
    {
        private static ActorSystem _myActorSystem;

        static void Main(string[] args)
        {
            _myActorSystem = ActorSystem.Create("MyActorSystem");

            var actor = _myActorSystem.ActorOf(Props.Create(() => new HealthCheckerActor()), "HealthCheckEk1Actor");
            actor.Tell("start");

            Console.ReadLine();
        }
    }
}
