﻿using Akka.Actor;

namespace StayingAlive.WinService.Checker
{
    public class Ek1Actor : UntypedActor
    {
        private readonly HealthManager _healthManager;

        public Ek1Actor(HealthManager healthManager)
        {
            _healthManager = healthManager;
        }

        protected override void OnReceive(object message)
        {
            if (message.Equals("Check"))
            {
                Context.Parent.Tell(_healthManager.IsHealthy());
            }
        }
    }
}