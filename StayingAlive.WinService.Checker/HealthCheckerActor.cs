﻿using System;
using Akka.Actor;

namespace StayingAlive.WinService.Checker
{
    public class HealthCheckerActor : UntypedActor
    {

        protected override void OnReceive(object message)
        {
            if (message.Equals("start"))
            {
                var actor = Context.ActorOf(Props.Create(() => new Ek1Actor(new HealthManager())));
                actor.Tell("Check");
            }
            else
            {
                Console.WriteLine(message);
            }
        }
        //public HealthCheckerActor()
        //{
        //    Receive<IHealthInformer>(message =>
        //    {
        //        Context.ActorOf(Props.Create(() => new HealthCheckEk1Actor()));
        //    });
        //}
    }
}
