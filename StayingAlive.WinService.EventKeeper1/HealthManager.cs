﻿using Common;
using System;

namespace StayingAlive.WinService.EventKeeper1
{
    public class HealthManager : IHealthInformer
    {
        public bool IsHealthy
        {
            get
            {
                var rndNUmber = new Random().Next(100);
                return rndNUmber >= 20 && rndNUmber <= 80;
            }   
        }
    }
}