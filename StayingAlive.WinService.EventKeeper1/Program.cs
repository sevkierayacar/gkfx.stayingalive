﻿using System;
using Akka.Actor;
using Common;

namespace StayingAlive.WinService.EventKeeper1
{
    class Program
    {
        private static ActorSystem _myActorSystem;

        static void Main(string[] args)
        {
            _myActorSystem = ActorSystem.Create("MyActorSystem");

            var healthCheckEk1Actor = _myActorSystem.ActorOf(Props.Create(() => new HealthCheckEk1Actor()), "HealthCheckEk1Actor");


            Console.ReadLine();
        }
    }
}
