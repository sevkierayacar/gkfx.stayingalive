﻿using Akka.Actor;

namespace Common
{
    public class HealthCheckEk1Actor : UntypedActor
    {
        private readonly IActorRef _healtActor;

        public HealthCheckEk1Actor(IActorRef healtActor)
        {
            _healtActor = healtActor;
        }

        protected override void OnReceive(object message)
        {
            _healtActor.Tell("check");
        }
    }
}