﻿namespace Common
{
    public interface IHealthInformer
    {
        bool IsHealthy { get; }
    }
}
